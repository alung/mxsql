//
//  MXObject.h
//
//  Created by longminxiang on 13-10-8.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXCondition.h"

@interface MXObject : NSObject

/* the object id */
@property (nonatomic, assign, readonly) int index;

/* the time of the object created */
@property (nonatomic, strong, readonly) NSDate *createTime;

/* the time of the object updated */
@property (nonatomic, strong, readonly) NSDate *updateTime;

/*!
 in here,you can set the properties which you hope not to add in to the table
 @param the supported type is NSString,NSDate,NSNumber,int,bool,float,double，
 any other type will ignore automatically
 */
+ (void)setIgnoreProperties:(NSArray *)propertyNames;

/* return the database path */
+ (NSString *)databaseFullPath;

/* set the database path */
+ (void)setDatabasePath:(NSString *)path directory:(NSSearchPathDirectory)directory;

/*!
 return the fields name of the table 
 @param it will return nil unless you save an object to db
 */
+ (NSArray *)fieldNames;

/*! 
 save object synchronously 
 @param don't use this method to save the object in a for-cycle or a while-cycle.
 if you have many objects to save,please use
 "+ (void)saveObjects:(NSArray *)objects completion:(void (^)(void))completion;"
 */
- (void)save;

/*!
 save objects asynchronously
 */
+ (void)saveObjects:(NSArray *)objects completion:(void (^)(void))completion;

/* get all object asynchronously */
+ (void)queryAllWithcompletion:(void (^)(NSArray *objects))completion;

/* get object by index asynchronously */
+ (void)queryByIndex:(int)index completion:(void (^)(id object))completion;

/*!
 get object by conditions asynchronously
 @param conditions include equal,less,greater,limit,order,see the "MXCondition.h"
 @param if your property type isn't an id object,
 please add the "@" to make sure it's an object,like this:@5,@YES,@5.55
 */
+ (void)queryWithCompletion:(void (^)(NSArray *objects))completion
                 conditions:(MXCondition *)condition,...NS_REQUIRES_NIL_TERMINATION;

/* return the counts of the record */
+ (int)queryCount;

/* delete the object */
- (BOOL)delete;

@end