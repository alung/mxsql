//
//  MXObject.m
//
//  Created by longminxiang on 13-10-8.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "MXObject.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"
#import <objc/runtime.h>

#define OBJECT_ID_NAME @"index"

#define DEFAULT_DB_PATH @"MXObject/MXDB"

#define DB_TABLE_NAME [[self class] description]

#define DB_FIELD_BEGAN_INDEX 9999

#define MXTString   @"text"
#define MXTDate     @"date"
#define MXTNumber   @"number"
#define MXTInt      @"integer"
#define MXTFloat    @"float"
#define MXTDouble   @"double"
#define MXTBOOL     @"boolean"

#pragma mark
#pragma mark ============
#pragma mark === MXField ===

//数据库字段类，同时也是property的映射
@interface MXField : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, strong) id value;

+ (NSArray *)fieldsForObject:(id)object superClass:(Class)supClass ignoreFields:(NSArray *)ignoreFields;

+ (NSArray *)fieldsNameForClass:(Class)class superClass:(Class)supClass ignoreFields:(NSArray *)ignoreFields;

@end

@implementation MXField

//解析类型
+ (NSString *)sqlTypeFromProperty:(objc_property_t)propertie
{
    NSString *propertyType = [NSString stringWithUTF8String:property_getAttributes(propertie)];
    propertyType = [[propertyType componentsSeparatedByString:@","] objectAtIndex:0];
    if ([propertyType isEqualToString:@"T@\"NSString\""]) return MXTString;
    if ([propertyType isEqualToString:@"T@\"NSDate\""]) return MXTDate;
    if ([propertyType isEqualToString:@"T@\"NSNumber\""]) return MXTNumber;
    if ([propertyType isEqualToString:@"Ti"]) return MXTInt;
    if ([propertyType isEqualToString:@"Tl"]) return MXTInt;
    if ([propertyType isEqualToString:@"Tf"]) return MXTFloat;
    if ([propertyType isEqualToString:@"Td"]) return MXTDouble;
    if ([propertyType isEqualToString:@"Tc"]) return MXTBOOL;
    return nil;
}

+ (BOOL)stringArray:(NSArray *)array isContainString:(NSString *)string
{
    for (id str in array) {
        if ([str isKindOfClass:[NSString class]]) {
            if ([string isEqualToString:str]) {
                return YES;
            }
        }
    }
    return NO;
}

//取本类及父类的属性变量及其值
+ (NSArray *)fieldsForObject:(id)object superClass:(Class)supClass ignoreFields:(NSArray *)ignoreFields
{
    return [self fieldsForObjectOrClass:object isObject:YES superClass:supClass ignoreFields:ignoreFields];
}

//取本类及父类的属性变量名
+ (NSArray *)fieldsNameForClass:(Class)class superClass:(Class)supClass ignoreFields:(NSArray *)ignoreFields
{
    return [self fieldsForObjectOrClass:class isObject:NO superClass:supClass ignoreFields:ignoreFields];
}

+ (NSArray *)fieldsForObjectOrClass:(id)objectOrClass
                           isObject:(BOOL)isObject
                         superClass:(Class)supClass
                       ignoreFields:(NSArray *)ignoreFields
{
    NSMutableArray *fieldArray = [NSMutableArray new];
    Class class = isObject ? [objectOrClass class] : objectOrClass;
    while ([class isSubclassOfClass:supClass]) {
        u_int count;
        objc_property_t *properties = class_copyPropertyList(class, &count);
        for (int i = 0; i < count; i++) {
            MXField *field = [MXField new];
            objc_property_t property = properties[i];
            NSString *type = [self sqlTypeFromProperty:property];
            NSString *propertyName = [NSString stringWithUTF8String: property_getName(property)];
            BOOL isIgnore = [self stringArray:ignoreFields isContainString:propertyName];
            if (type && !isIgnore) {
                field.type = type;
                field.name = propertyName;
                if (isObject) {
                    id value = [objectOrClass valueForKey:propertyName];
                    field.value = value;
                }
                [fieldArray addObject:field];
            }
        }
        free(properties);
        class = [class superclass];
    }
    return fieldArray;
}

@end

#pragma mark
#pragma mark =============
#pragma mark === MXObject ===

@interface MXObject ()

@property (nonatomic, strong) NSMutableArray *saveQueueArray;
@property (nonatomic, assign) BOOL lockSaveQueue;

@end

static NSArray *_ignoreProperties;
static NSString *_fullPath;

@implementation MXObject

+ (id)sharedObject
{
    return [[self class] new];
}

+ (void)initialize
{
    [super initialize];
    [self setIgnoreProperties:@[@"lockSaveQueue",@"saveQueueArray"]];
    [self setDatabasePath:DEFAULT_DB_PATH directory:NSCachesDirectory];
}

+ (void)setIgnoreProperties:(NSArray *)propertyNames
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:propertyNames];
    Class superClass = [self superclass];
    while ([superClass isSubclassOfClass:[MXObject class]]) {
        for (id ob in [superClass ignoreProperties]) {
            if (![array containsObject:ob]) {
                [array addObject:ob];
            }
        }
        superClass = [superClass superclass];
    }
    _ignoreProperties = array;
}

+ (NSArray *)ignoreProperties
{
    return _ignoreProperties;
}

+ (void)setDatabasePath:(NSString *)path directory:(NSSearchPathDirectory)directory
{
    NSString *dir = NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES)[0];
    NSArray *pathArr = [path componentsSeparatedByString:@"/"];
    if (pathArr.count > 1) {
        NSString *rdir = [dir stringByAppendingPathComponent:[path substringToIndex:path.length - [pathArr.lastObject length] - 1]];
        [[NSFileManager defaultManager] createDirectoryAtPath:rdir withIntermediateDirectories:YES attributes:nil error:NULL];
        path = [rdir stringByAppendingPathComponent:pathArr.lastObject];
    }
    else {
        path = [dir stringByAppendingPathComponent:path];
    }
    _fullPath = path;
}

+ (NSString *)databaseFullPath
{
    return _fullPath;
}

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (void)setIndex:(int)index
{
    _index = index;
}

- (void)setCreateTime:(NSDate *)createTime
{
    _createTime = createTime;
}

- (void)setUpdateTime:(NSDate *)updateTime
{
    _updateTime = updateTime;
}

- (NSArray *)fieldObjects
{
    return [MXField fieldsForObject:self superClass:[MXObject class] ignoreFields:[[self class] ignoreProperties]];
}

- (NSArray *)fieldNames
{
    NSMutableArray *fields = nil;
    FMDatabase *db = [self dataBase];
    if (![db open]) return fields;
    
    NSString *sql = @"SELECT * FROM %@ LIMIT 1";
    sql = [self SQL:sql];
    [db setLogsErrors:NO];
    FMResultSet *rs = [db executeQuery:sql];
    [db setLogsErrors:YES];
    if (rs) {
        fields = [NSMutableArray new];
        for (int i = 0; i < [rs columnCount]; i++) {
            NSString *str = [[NSString alloc] initWithUTF8String:sqlite3_column_name(rs.statement.statement,i)];
            [fields addObject:str];
        }
        if (rs.statement.statement) {
            sqlite3_finalize(rs.statement.statement);
            rs.statement.statement = 0x00;
        }
        [rs close];
        [db close];
    }
    return fields;
}

+ (NSArray *)fieldNames
{
    return [[self sharedObject] fieldNames];
}

#pragma mark
#pragma mark === FMDatabase ===

- (FMDatabase *)dataBase
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] databaseFullPath]];
    return db;
}

- (FMDatabaseQueue *)queue
{
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:[[self class] databaseFullPath]];
    return queue;
}

- (NSString *)SQL:(NSString *)sql
{
    return [NSString stringWithFormat:sql,DB_TABLE_NAME];
}

- (void)createTableWithDB:(FMDatabase *)db
{
    if ([db tableExists:DB_TABLE_NAME]) return;
    NSArray *fields = [self fieldObjects];
    NSString *sql = [self SQL:@"CREATE TABLE IF NOT EXISTS '%@' ("];
    for (int i = 0; i < fields.count; i++) {
        MXField *field = [fields objectAtIndex:i];
        NSString *fString = (i == fields.count - 1) ? @")" : @",";
        sql = [sql stringByAppendingFormat:@"'%@' %@%@",field.name,field.type,fString];
    }
    [db executeUpdate:sql];
}

- (void)updateTableIfNeeded
{
    NSArray *originFields = [self fieldNames];
    if (originFields.count > 0) {
        NSMutableArray *newFields = [NSMutableArray new];
        for (MXField *newField in [self fieldObjects]) {
            BOOL has = NO;
            for (NSString *originField in originFields) {
                if ([newField.name isEqualToString:originField]) {
                    has = YES;break;
                }
            }
            if (!has) [newFields addObject:newField];
        }
        if (newFields.count > 0) {
            [self addFields:newFields];
        }
    }
}

- (void)addFields:(NSArray *)fields
{
    FMDatabaseQueue *queue = [self queue];
    [queue inDatabase:^(FMDatabase *db) {
        for (int i = 0; i < fields.count; i++) {
            MXField *field = [fields objectAtIndex:i];
            NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %%@ ADD COLUMN '%@' %@ DEFAULT '%@'",field.name,field.type,field.value];
            sql = [self SQL:sql];
            [db executeUpdate:sql];
        }
    }];
}

#pragma mark ==== save ====

- (int)maxIndexWithDB:(FMDatabase *)db
{
    int index = 0;
    NSString *sql = [NSString stringWithFormat:@"SELECT max(\"%@\") FROM %%@",OBJECT_ID_NAME];
    sql = [self SQL:sql];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next]) {
        index = [rs intForColumnIndex:0];
    }
    [rs close];
    if (index < DB_FIELD_BEGAN_INDEX) {
        index = DB_FIELD_BEGAN_INDEX;
    }
    return index;
}

//更新
- (void)updateWithDB:(FMDatabase *)db
{
    self.updateTime = [NSDate date];
    NSArray *fields = [self fieldObjects];
    NSString *sql = [self SQL:@"UPDATE %@ SET "];
    int fieldsCount = fields.count;
    for (int i = 0; i < fieldsCount; i++) {
        MXField *field = [fields objectAtIndex:i];
        NSString *fString = (i == fieldsCount - 1) ? @"" : @",";
        sql = [sql stringByAppendingFormat:@"'%@' = '%@'%@",field.name,field.value,fString];
    }
    sql = [sql stringByAppendingFormat:@" WHERE \"%@\" = ?",OBJECT_ID_NAME];
    [db executeUpdate:sql,[NSNumber numberWithInt:self.index]];
}

//无条件强势插入
- (void)saveOnlyWithDB:(FMDatabase *)db
{
    self.index = [self maxIndexWithDB:db];
    self.index++;
    self.createTime = [NSDate date];
    self.updateTime = [NSDate date];
    
    NSArray *fields = [self fieldObjects];
    NSString *sql = [self SQL:@"INSERT OR IGNORE INTO %@ ("];
    NSString *vFlag = @" VALUES (";
    for (int i = 0; i < fields.count; i++) {
        MXField *field = [fields objectAtIndex:i];
        NSString *fString = (i == fields.count - 1) ? @")" : @",";
        sql = [sql stringByAppendingFormat:@"'%@'%@",field.name,fString];
        vFlag = [vFlag stringByAppendingFormat:@"'%@'%@",field.value,fString];
    }
    sql = [sql stringByAppendingString:vFlag];
    [db executeUpdate:sql];
}

//插入
- (void)saveWithDB:(FMDatabase *)db
{
    //查询是否已有记录
    [db setLogsErrors:NO];
    id object = [self queryByIndex:self.index db:db];
    [db setLogsErrors:YES];
    MXObject *ob = (MXObject *)object;
    //如有，执行更新操作
    if (ob && ob.index == self.index) {
        [self updateWithDB:db];
        return;
    }
    //查询是否已建表，如无，执行建表操作
    [self createTableWithDB:db];
    
    //查询是否有新加列，如有，执行添加列操作
    [self updateTableIfNeeded];
    
    //插入
    [self saveOnlyWithDB:db];
}

- (void)startSaveQueue
{
    if (self.saveQueueArray.count > 0 && !self.lockSaveQueue) {
        self.lockSaveQueue = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSDictionary *dic = [self.saveQueueArray objectAtIndex:0];
            NSArray *array = [dic objectForKey:@"array"];
            void (^block)(void) = [dic objectForKey:@"block"];
            FMDatabaseQueue *queue = [self queue];
            [queue inDatabase:^(FMDatabase *db) {
                for (int i = 0 ; i < array.count; i++) {
                    id ob = [array objectAtIndex:i];
                    if ([ob isKindOfClass:[self class]]) {
                        [ob saveWithDB:db];
                    }
                }
                self.lockSaveQueue = NO;
                block();
                [self.saveQueueArray removeObjectAtIndex:0];
                [self startSaveQueue];
            }];
        });
    }
}

//插入数组
- (void)saveObjects:(NSArray *)objects completion:(void (^)(void))completion
{
    if (!self.saveQueueArray) {
        self.saveQueueArray = [NSMutableArray new];
    }
    void (^__strong co)(void)= completion;
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:objects,@"array",co,@"block", nil];
    [self.saveQueueArray addObject:dic];
    [self startSaveQueue];
}

+ (void)saveObjects:(NSArray *)objects completion:(void (^)(void))completion
{
    if (!objects) return;
    [[self sharedObject] saveObjects:objects completion:completion];
}

- (void)save
{
    FMDatabase *db = [self dataBase];
    if (![db open]) return;
    [self saveWithDB:db];
    [db close];
}

#pragma mark ==== query ====

//通过ID查询
- (id)queryByIndex:(int)index db:(FMDatabase *)db
{
    id ob = nil;
    NSString *sql = [NSString stringWithFormat:@"SELECT * from %%@ WHERE \"%@\" = ?",OBJECT_ID_NAME];
    sql = [self SQL:sql];
    FMResultSet *rs = [db executeQuery:sql,[NSNumber numberWithInt:self.index]];
    while ([rs next]) {
        ob = [self objectFromFMResultSet:rs];
    }
    [rs close];
    return ob;
}

//查询
- (NSArray *)queryWithDB:(FMDatabase *)db condition:(NSString *)conditionString
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %%@%@",conditionString];
    sql = [self SQL:sql];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next]) {
        id ob = [self objectFromFMResultSet:rs];
        [result addObject:ob];
    }
    [rs close];
    return result;
}

- (void)queryWithCompletion:(void (^)(NSArray *))completion condition:(NSString *)conditionString
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *result = nil;
        FMDatabase *db = [self dataBase];
        if ([db open]) {
            result = [self queryWithDB:db condition:conditionString ? conditionString : @""];
            [db close];
        }
        completion (result);
    });
}

//条件查询
+ (void)queryWithCompletion:(void (^)(NSArray *))completion conditions:(MXCondition *)condition, ...NS_REQUIRES_NIL_TERMINATION
{
    NSMutableArray *conditions = [NSMutableArray new];
    va_list args;
    va_start(args, condition);
    while (condition) {
        [conditions addObject:condition];
        condition = va_arg(args, id);
    }
    va_end(args);
    NSString *conditionString = [MXCondition conditionStringWithConditions:conditions];
    [[self sharedObject] queryWithCompletion:completion condition:conditionString];
}

//查询所有
- (void)queryAllWithcompletion:(void (^)(NSArray *objects))completion
{
    [self queryWithCompletion:completion condition:nil];
}

//查询所有，类方法，对外接口
+ (void)queryAllWithcompletion:(void (^)(NSArray *objects))completion
{
    return [[self sharedObject] queryAllWithcompletion:completion];
}

+ (void)queryByIndex:(int)index completion:(void (^)(id object))completion
{
    [self queryWithCompletion:^(NSArray *objects){
        if (objects.count > 0) {
            completion (objects[0]);
        }
    } conditions:
     [MXCondition whereKey:OBJECT_ID_NAME equalTo:[NSNumber numberWithInt:index]], nil];
}

//查询结果转换函数
- (id)objectFromFMResultSet:(FMResultSet *)rs
{
    id ob = [[self class] new];
    NSArray *fields = [self fieldObjects];
    for (int i = 0; i < fields.count; i++) {
        MXField *field = [fields objectAtIndex:i];
        [ob setValue:[rs objectForColumnName:field.name] forKey:field.name];
    }
    return ob;
}

//查询数量
- (int)queryCount
{
    int count = 0;
    FMDatabase *db = [self dataBase];
    if (![db open]) return count;
    
    NSString *sql = [self SQL:@"SELECT COUNT(*) FROM %@"];
    FMResultSet *rs = [db executeQuery:sql];
    while ([rs next]) {
        count = [rs intForColumnIndex:0];
    }
    [rs close];
    [db close];
    return count;
}

//查询数量，类方法，对外接口
+ (int)queryCount
{
    return [[self sharedObject] queryCount];
}

#pragma mark ==== delete ====

- (BOOL)deleteWithDB:(FMDatabase *)db
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %%@ WHERE \"%@\"",OBJECT_ID_NAME];
    sql = [self SQL:sql];
    return [db executeUpdate:sql];
}

- (BOOL)delete
{
    BOOL success = NO;
    FMDatabase *db = [self dataBase];
    if (![db open]) return success;
    success = [self deleteWithDB:db];
    [db close];
    return success;
}

@end
