//
//  ViewController.m
//  MXSQLDemo
//
//  Created by longminxiang on 13-10-8.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "ViewController.h"
#import "Man.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(60, 80, 200, 44)];
    [button setTitle:@"touch see see" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)buttonTouched:(id)sender
{
    Man *diaosi = [Man new];
    diaosi.name = @"a san";
    diaosi.age = 30;
    diaosi.gfs = NO;
    [diaosi save];
    [Man queryByIndex:diaosi.index completion:^(id object) {
        Man *ds = (Man *)object;
        NSLog(@"diaosi %d born at %@",ds.index,ds.createTime);
    }];
    
    Man *gaofusuai = [Man new];
    gaofusuai.name = @"wlh";
    gaofusuai.money = 1000000000;
    gaofusuai.age = 28;
    gaofusuai.gfs = YES;
    [gaofusuai save];
    NSLog(@"gaofusuai %d born at %@",gaofusuai.index,gaofusuai.createTime);
    
    gaofusuai.houses = [NSMutableArray new];
    for (int i = 0; i < 100; i++) {
        House *house = [House new];
        house.ownerIndex = gaofusuai.index;
        house.name = [NSString stringWithFormat:@"house %d",i];
        house.value = 5000000;
        [gaofusuai.houses addObject:house];
    }
    [House saveObjects:gaofusuai.houses completion:^{
        [House queryWithCompletion:^(NSArray *objects) {
            for (int i = 0; i < objects.count; i++) {
                House *hs = (House *)objects[i];
                NSLog(@"gaofusuai %d bought house %d",gaofusuai.index,hs.index);
            }
        } conditions:
         [MXCondition whereKey:@"ownerIndex" equalTo:[NSNumber numberWithInt:gaofusuai.index]],
         nil];
    }];
    
    [Man queryAllWithcompletion:^(NSArray *objects) {
        for (Man *man in objects) {
            NSLog(@"man %d %@",man.index,man.gfs ? @"is gaofusuai" : @"is diaosi");
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
