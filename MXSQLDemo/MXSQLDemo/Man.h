//
//  Man.h
//  MXSQLDemo
//
//  Created by longminxiang on 13-10-10.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "MXObject.h"

@interface Man : MXObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int age;
@property (nonatomic, assign) double money;
@property (nonatomic, assign) BOOL gfs;
@property (nonatomic, strong) NSMutableArray *houses;

@end

@interface House : MXObject

@property (nonatomic, assign) int ownerIndex;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int value;

@end
