#MXSQL

This is a sql data to NSObject mapper base on [FMDB](https://github.com/ccgus/fmdb).

It allows you save NSObject to database simplicity.

#Usage

let your object class inherit MXObject

	@interface Man : MXObject

	@property (nonatomic, copy) NSString *name;
	@property (nonatomic, assign) int age;
	@property (nonatomic, assign) double money;
	@property (nonatomic, assign) BOOL gfs;
	@property (nonatomic, strong) NSMutableArray *houses;

	@end
	
and than you can new one save it

	Man *diaosi = [Man new];
	[diaosi save];
	
#License

MXSQL is available under the MIT license. See the LICENSE file for more info. 
If you are using MXSQL in your project, please me know at longminxiang@163.com.